<form>
  <fieldset>
    <h1 class="text-center">Moving Details</h1>
    <div class="form-group">
      <label class=" control-label" for="radios">What day would you like to move?</label>
      <div class=""> 
        <label class="radio-inline" for="radios-0">
          <input name="radios" id="radios-0" value="1" checked="checked" type="radio">
          Sun
        </label> 
        <label class="radio-inline" for="radios-1">
          <input name="radios" id="radios-1" value="2" type="radio">
          Mon
        </label> 
        <label class="radio-inline" for="radios-2">
          <input name="radios" id="radios-2" value="3" type="radio">
          Tue
        </label> 
        <label class="radio-inline" for="radios-3">
          <input name="radios" id="radios-3" value="4" type="radio">
          Wed
        </label> 
        <label class="radio-inline" for="radios-4">
          <input name="radios" id="radios-4" value="5" type="radio">
          Thu
        </label>
        <label class="radio-inline" for="radios-5">
          <input name="radios" id="radios-5" value="6" type="radio">
          Fri
        </label>
        <label class="radio-inline" for="radios-6">
          <input name="radios" id="radios-6" value="7" type="radio">
          Sat
        </label>
      </div>
    </div>

    <div class="form-group">
      <div class="">
        <label class=" control-label" for="textinput">If your date is not available what day will like as 2nd option?</label>
        <input id="date" name="textinput" placeholder="Date" class="form-control input-md" type="text">
      </div>
    </div>
    
    <div class="form-group">
      <div class="">
        <label class=" control-label" for="textinput">What time will you like to move?</label>
        <input id="time" name="textinput" placeholder="Time" class="form-control input-md" type="text">
        <span class="help-block small">*We always give our drivers a 2 hour window to arrival at your current address  </span>
      </div>
    </div>

    <div class="form-group">
      <label class=" control-label" for="origin">Origin Address: </label>
      <div class="">
        <textarea class="form-control" id="origin" name="origin"></textarea>
      </div>
    </div>

    <div class="form-group">
      <label class=" control-label h6" for="destination">Destination Address: </label>
      <div class="">
        <textarea class="form-control" id="destination" name="destination"></textarea>
      </div>
    </div>

    <div class="form-group">
      <label class=" control-label" for="selectbasic">Estimated Weight:</label>
      <div class="">
        <select id="selectbasic" name="selectbasic" class="form-control">
          <option value="1">Studio Apt. = 1,500 lbs</option>
          <option value="2">1 Bedroom = 3,000 lbs</option>
          <option value="3">2 Bedroom = 6,000 lbs</option>
          <option value="4">3 Bedroom = 9,000 lbs</option>
          <option value="5">3 bedroom + Garage  = 10,000 lbs</option>
          <option value="6">4+ Bedrooms</option>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label class=" control-label" for="movers">How many mover do you require?</label>
      <div class=""> 
        <label class="radio-inline" for="movers-0">
          <input name="movers" id="movers-0" value="1" checked="checked" type="radio">
            1
        </label> 
        <label class="radio-inline" for="movers-1">
          <input name="movers" id="movers-1" value="2" type="radio">
          2
        </label> 
        <label class="radio-inline" for="movers-2">
          <input name="movers" id="movers-2" value="3" type="radio">
          3
        </label> 
        <label class="radio-inline" for="movers-3">
          <input name="movers" id="movers-3" value="4" type="radio">
          4
        </label> 
        <label class="radio-inline" for="movers-4">
          <input name="movers" id="movers-4" value="5" type="radio">
          5
        </label>
        <label class="radio-inline" for="movers-5">
          <input name="movers" id="movers-5" value="6" type="radio">
          6
        </label>
        <label class="radio-inline" for="movers-6">
          <input name="movers" id="movers-6" value="7" type="radio">
          7
        </label>
      </div>
    </div>

    <div class="form-group">
      <label class=" control-label" for="truck_size">What size truck do you require?</label>
      <div class="">
        <label class="radio-inline" for="truck_size_0">
            <input name="truck_size" id="truck_size_0" value="1" checked="checked" type="radio">
            26 ft.
        </label> 
        <label class="radio-inline" for="truck_size_1">
          <input name="truck_size" id="truck_size_1" value="2" type="radio">
          28 ft.
        </label> 
        <label class="radio-inline" for="truck_size_2">
          <input name="truck_size" id="truck_size_2" value="3" type="radio">
          30 ft.
        </label> 
        <label class="radio-inline" for="truck_size_3">
          <input name="truck_size" id="truck_size_3" value="4" type="radio">
          Tracker Trailer
        </label> 
      </div>
    </div>

    <div class="form-group">
      <label class=" control-label" for="extra_stops">Do you need to make any extra stops?</label>
      <div class=""> 
        <label class="radio-inline" for="extra_stops-0">
          <input name="extra_stops" id="extra_stops-0" value="1" checked="checked" type="radio">
            Yes
        </label> 
        <label class="radio-inline" for="extra_stops-1">
          <input name="extra_stops" id="appliance-1" value="2" type="radio">
          No
        </label> 
      </div>
    </div>

    <div class="form-group">
      <span class="help-block small">*If yes, how many?</span>
      <div class=""> 
        <label class="radio-inline" for="extra_stops_no-0">
          <input name="extra_stops_no" id="extra_stops_no-0" value="1" checked="checked" type="radio">
            1
        </label> 
        <label class="radio-inline" for="extra_stops_no-1">
          <input name="extra_stops_no" id="extra_stops_no-1" value="2" type="radio">
          2
        </label> 
        <label class="radio-inline" for="extra_stops_no-1">
          <input name="extra_stops_no" id="extra_stops_no-1" value="2" type="radio">
          3
        </label> 
      </div>
    </div>

    <div class="form-group">
      <label class=" control-label" for="packing">Do you require packing services?</label>
      <div class=""> 
        <label class="radio-inline" for="packing-0">
          <input name="packing" id="packing-0" value="1" checked="checked" type="radio">
            Yes
        </label> 
        <label class="radio-inline" for="packing-1">
          <input name="packing" id="packing-1" value="2" type="radio">
          No
        </label> 
      </div>
    </div>

    <div class="form-group">
      <label class=" control-label" for="storage">Do you require storage?</label>
      <div class=""> 
        <label class="radio-inline" for="storage-0">
          <input name="storage" id="storage-0" value="1" checked="checked" type="radio">
            Yes
        </label> 
        <label class="radio-inline" for="storage-1">
          <input name="storage" id="storage-1" value="2" type="radio">
          No
        </label> 
      </div>
    </div>

    <div class="form-group">
      <label class=" control-label" for="appliance">Are you moving any Appliances?</label>
      <div class=""> 
        <label class="radio-inline" for="appliance-0">
          <input name="appliance" id="appliance-0" value="1" checked="checked" type="radio">
            Yes
        </label> 
        <label class="radio-inline" for="appliance-1">
          <input name="appliance" id="appliance-1" value="2" type="radio">
          No
        </label> 
      </div>
    </div>

    <div class="form-group">
      <span class="help-block small">*If yes check all that apply</span>
      <div class="">
        <select id="selectmultiple" name="selectappliance" class="form-control" multiple="multiple">
          <option value="1">Washer</option>
          <option value="2">Dryer</option>
          <option value="3">Stove</option>
          <option value="4">Deep Freezer</option>
          <option value="5">Refrigerator</option>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label class=" control-label" for="assembly">Do you require any Disassembly or Reassembly?</label>
      <div class=""> 
        <label class="radio-inline" for="assembly-0">
          <input name="assembly" id="assembly-0" value="1" checked="checked" type="radio">
            Yes
        </label> 
        <label class="radio-inline" for="assembly-1">
          <input name="assembly" id="assembly-1" value="2" type="radio">
          No
        </label> 
      </div>
    </div>

    <div class="form-group">
      <label class=" control-label" for="piano_gun_safe">Are you moving a Piano or Gun Safe?</label>
      <div class=""> 
        <label class="radio-inline" for="piano_gun_safe-0">
          <input name="piano_gun_safe" id="piano_gun_safe-0" value="1" checked="checked" type="radio">
            Yes
        </label> 
        <label class="radio-inline" for="piano_gun_safe-1">
          <input name="piano_gun_safe" id="assembly-1" value="2" type="radio">
          No
        </label> 
      </div>
    </div>

    <div class="form-group">
      <div class="">
        <button id="singlebutton" name="singlebutton" class="btn btn-success btn-lg btn-block">Proceed To Step 3</button>
      </div>
    </div>
  </fieldset>
</form>