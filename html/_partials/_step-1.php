<form>
  <fieldset>
    <h1 class="text-center">Customer Information</h1>
    <div class="form-group">
      <div class="">
        <input id="textinput" name="textinput" placeholder="First Name" class="form-control input-md" type="text">
      </div>
    </div>

    <div class="form-group">
      <div class="">
        <input id="textinput" name="textinput" placeholder="Last Name" class="form-control input-md" type="text">
      </div>
    </div>

    <div class="form-group">
      <div class="">
        <input id="textinput" name="textinput" placeholder="Email Address" class="form-control input-md" type="text">
      </div>
    </div>

    <div class="form-group">
      <div class="">
        <input id="textinput" name="textinput" placeholder="Phone Number" class="form-control input-md" type="text">
      </div>
    </div>

    <div class="form-group">
      <label class=" control-label" for="radios">Best Time To Call</label>
      <div class="">
        <select id="selectbasic" name="selectbasic" class="form-control">
          <option value="1">Any</option>
          <option value="2">Morning</option>
          <option value="2">Afternoon</option>
          <option value="2">Night</option>
        </select>
      </div>
    </div>

    <div class="form-group">
      <div class="">
        <button id="singlebutton" name="singlebutton" class="btn btn-success btn-lg btn-block">Proceed To Step 2</button>
      </div>
    </div>
  </fieldset>
</form>