<form>
  <fieldset>
    <h1 class="text-center">Payment</h1><span class="text-center help-block">Information must match your card</span>


    <div class="form-group">
      <div class="">
        <input id="textinput" name="textinput" placeholder="First Name" class="form-control input-md" type="text">
      </div>
    </div>

    <div class="form-group">
      <div class="">
        <input id="textinput" name="textinput" placeholder="Last Name" class="form-control input-md" type="text">
      </div>
    </div>

    <div class="form-group">
      <label class=" control-label" for="radios">Card Type</label>
      <div class=""> 
        <label class="radio-inline" for="radios-0">
          <input name="radios" id="radios-0" value="1" checked="checked" type="radio">
          Visa
        </label> 
        <label class="radio-inline" for="radios-1">
          <input name="radios" id="radios-1" value="2" type="radio">
          Mastercard
        </label>
      </div>
      <span class="help-block small">*We do not accept Prepaid/Discover/Amex cards.</span>
    </div>

    <div class="form-group">
      <div class="">
        <input id="textinput" name="textinput" placeholder="Card Number" class="form-control input-md" type="text">
      </div>
    </div>

    <div class="form-group">
      <div class="">
        <input id="card_exp" name="card_exp" placeholder="Card Expiry (MM/YY)" class="form-control input-md" type="text">
      </div>
    </div>

    <div class="form-group">
      <div class="">
        <input id="textinput" name="textinput" placeholder="CVV" class="form-control input-md" type="text">
      </div>
    </div>

    <div class="form-group">
      <label class="control-label" for="destination">Billing Address:</label>
      <div class="">
        <textarea class="form-control" id="destination" name="destination"></textarea>
      </div>
    </div>

    <div class="form-group">
      <div class="">
        <input id="textinput" name="textinput" placeholder="Email Address" class="form-control input-md" type="text">
      </div>
    </div>

    <div class="form-group">
      <div class="">
        <input id="textinput" name="textinput" placeholder="Phone Number" class="form-control input-md" type="text">
      </div>
    </div>

    <div class="form-group">
      <div class="">
        <label class="control-label" for="destination">Driver License of person paying for the move (Driver Will Verify)</label>
        <input id="textinput" name="textinput" placeholder="Driver License #" class="form-control input-md" type="text">
      </div>
    </div>

    <div class="form-group">
      <div class="">
        <input id="textinput" name="textinput" placeholder="State" class="form-control input-md" type="text">
      </div>
    </div>

    <div class="form-group">
      <div class="">
        <input id="lic_issue" name="lic_issue" placeholder="Issue Date" class="form-control input-md" type="text">
      </div>
    </div>

    <div class="form-group">
      <div class="">
        <input id="lic_exp" name="lic_exp" placeholder="Exp. Date" class="form-control input-md" type="text">
      </div>
    </div>


    <div class="form-group">
      <div class="">
        <button id="singlebutton" name="singlebutton" class="btn btn-success btn-lg btn-block">Proceed To Step 4</button>
      </div>
    </div>
  </fieldset>
</form>