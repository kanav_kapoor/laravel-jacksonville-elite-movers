<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Jacksonville Elite Movers</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
	<link rel="stylesheet" href="assets/css/app.css">
</head>
<body>
<div class="container">
	<div class="row form-group steps-row">
		<div class="col-xs-12">
			<ul class="nav nav-pills nav-justified thumbnail setup-panel">
				<li class="active"><a href="#step-1">
					<h4 class="list-group-item-heading">Step 1</h4>
					<p class="list-group-item-text">Customer Information</p>
				</a></li>
				<li><a href="#step-2">
					<h4 class="list-group-item-heading">Step 2</h4>
					<p class="list-group-item-text">Moving Details</p>
				</a></li>
				<li><a href="#step-3">
					<h4 class="list-group-item-heading">Step 3</h4>
					<p class="list-group-item-text">Payment</p>
				</a></li>
				<li><a href="#step-4">
					<h4 class="list-group-item-heading">Step 4</h4>
					<p class="list-group-item-text">Terms & Conditions</p>
				</a></li>
				<li><a href="#step-5">
					<h4 class="list-group-item-heading">Step 5</h4>
					<p class="list-group-item-text">Authorization</p>
				</a></li>
			</ul>
		</div>
	</div>
	<div class="row setup-content" id="step-1">
		<div class="col-xs-12">
			<div class="col-md-12">
				<div class="container">
			    <div id="big-form" class="well auth-box">
					<?php require '_partials/_step-1.php'; ?>
			    </div>
			    <div class="clearfix"></div>
			  </div>
			</div>
		</div>
	</div>

	<div class="row setup-content" id="step-2">
		<div class="col-xs-12">
			<div class="col-md-12">
				<div class="container">
			    <div id="big-form" class="well auth-box">
						<?php require '_partials/_step-2.php'; ?>
			    </div>
			    <div class="clearfix"></div>
			  </div>
			</div>
		</div>
	</div>

	<div class="row setup-content" id="step-3">
		<div class="col-xs-12">
			<div class="col-md-12">
				<div class="container">
			    <div id="big-form" class="well auth-box">
						<?php require '_partials/_step-3.php'; ?>
			    </div>
			    <div class="clearfix"></div>
			  </div>
			</div>
		</div>
	</div>

	<div class="row setup-content" id="step-4">
		<div class="col-xs-12">
			<div class="col-md-12">
				<div class="container">
			    <div id="big-form" class="well auth-box">
						<?php require '_partials/_step-4.php'; ?>
			    </div>
			    <div class="clearfix"></div>
			  </div>
			</div>
		</div>
	</div>

	<div class="row setup-content" id="step-5">
		<div class="col-xs-12">
			<div class="col-md-12">
				<div class="container">
			    <div id="big-form" class="well auth-box">
						<?php require '_partials/_step-5.php'; ?>
			    </div>
			    <div class="clearfix"></div>
			  </div>
			</div>
		</div>
	</div>
</div>
<script src="http://code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
<script src="assets/js/app.js"></script>
</body>
</html>