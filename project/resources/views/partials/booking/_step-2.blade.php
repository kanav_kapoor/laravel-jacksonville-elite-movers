<div class="row setup-content" id="step-2">
  <div class="col-xs-12">
    <div class="col-md-12">
      <div class="container">
        <div id="big-form" class="well auth-box">
            <fieldset>
              <h1 class="text-center">Moving Details</h1>
              {{-- <div class="form-group">
                <label class=" control-label" for="radios">What day would you like to move?</label>
                <div class=""> 
                  <label class="radio-inline" for="sun">
                    <input name="radios" id="sun" value="sun" checked="checked" type="radio">
                    Sun
                  </label> 
                  <label class="radio-inline" for="radios-1">
                    <input name="radios" id="radios-1" value="2" type="radio">
                    Mon
                  </label> 
                  <label class="radio-inline" for="radios-2">
                    <input name="radios" id="radios-2" value="3" type="radio">
                    Tue
                  </label> 
                  <label class="radio-inline" for="radios-3">
                    <input name="radios" id="radios-3" value="4" type="radio">
                    Wed
                  </label> 
                  <label class="radio-inline" for="radios-4">
                    <input name="radios" id="radios-4" value="5" type="radio">
                    Thu
                  </label>
                  <label class="radio-inline" for="radios-5">
                    <input name="radios" id="radios-5" value="6" type="radio">
                    Fri
                  </label>
                  <label class="radio-inline" for="radios-6">
                    <input name="radios" id="radios-6" value="7" type="radio">
                    Sat
                  </label>
                </div>
              </div> --}}

              <div class="form-group">
                <div class="">
                  <label class=" control-label" for="moving_date">What date would you like to move?</label>
                  <input id="moving_date" name="moving_date" placeholder="Date" class="form-control input-md" type="text">
                </div>
              </div>
              
              <div class="form-group">
                <div class="">
                  <label class=" control-label" for="moving_time">What time would you like to move?</label>
                  <input id="moving_time" name="moving_time" placeholder="Time" class="form-control input-md" type="text">
                </div>
                <span class="help-block small">*We always give our drivers a 2 hour window to arrival at your current address  </span>
              </div>

              <div class="form-group">
                <div class="">
                  <label class=" control-label" for="moving_date_alt">If your date is not available what day will like as 2nd option?</label>
                  <input id="moving_date_alt" name="moving_date_alt" placeholder="Date" class="form-control input-md" type="text">
                </div>
              </div>
              
              <div class="form-group">
                <div class="">
                  <label class=" control-label" for="moving_time_alt">Time for alternate date</label>
                  <input id="moving_time_alt" name="moving_time_alt" placeholder="Time" class="form-control input-md" type="text">
                </div>
                <span class="help-block small">*We always give our drivers a 2 hour window to arrival at your current address  </span>
              </div>

              <div class="form-group">
                <label class=" control-label" for="origin_address">Origin Address: </label>
                <div class="">
                  <textarea class="form-control" id="origin_address" name="origin"></textarea>
                </div>
              </div>

              <div class="form-group">
                <label class=" control-label h6" for="destination">Destination Address: </label>
                <div class="">
                  <textarea class="form-control" id="destination" name="destination"></textarea>
                </div>
              </div>

              <div class="form-group">
                <label class=" control-label" for="weight_id">Estimated Weight:</label>
                <div class="">
                  <select id="weight_id" name="weight_id" class="form-control">
                    <option value="1">Studio Apt. = 1,500 lbs</option>
                    <option value="2">1 Bedroom = 3,000 lbs</option>
                    <option value="3">2 Bedroom = 6,000 lbs</option>
                    <option value="4">3 Bedroom = 9,000 lbs</option>
                    <option value="5">3 bedroom + Garage  = 10,000 lbs</option>
                    <option value="6">4+ Bedrooms</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class=" control-label" for="movers_no">How many mover do you require?</label>
                <div class=""> 
                  <label class="radio-inline" for="movers_no_1">
                    <input name="movers_no" id="1" value="movers_no_1" checked="checked" type="radio">
                      1
                  </label> 
                  <label class="radio-inline" for="movers_no_2">
                    <input name="movers_no" id="movers_no_2" value="2" type="radio">
                    2
                  </label> 
                  <label class="radio-inline" for="movers_no_3">
                    <input name="movers_no" id="movers_no_3" value="3" type="radio">
                    3
                  </label> 
                  <label class="radio-inline" for="movers_no_4">
                    <input name="movers_no" id="movers_no_4" value="4" type="radio">
                    4
                  </label> 
                  <label class="radio-inline" for="movers_no_5">
                    <input name="movers_no" id="movers_no_5" value="5" type="radio">
                    5
                  </label>
                  <label class="radio-inline" for="movers_no_6">
                    <input name="movers_no" id="movers_no_6" value="6" type="radio">
                    6
                  </label>
                  <label class="radio-inline" for="movers_no_7">
                    <input name="movers_no" id="movers_no_7" value="7" type="radio">
                    7
                  </label>
                </div>
              </div>

              <div class="form-group">
                <label class=" control-label" for="truck_size_id">What size truck do you require?</label>
                <div class="">
                  <label class="radio-inline" for="truck_size_1">
                      <input name="truck_size_id" id="truck_size_1" value="1" checked="checked" type="radio">
                      26 ft.
                  </label> 
                  <label class="radio-inline" for="truck_size_2">
                    <input name="truck_size_id" id="truck_size_2" value="2" type="radio">
                    28 ft.
                  </label> 
                  <label class="radio-inline" for="truck_size_3">
                    <input name="truck_size_id" id="truck_size_3" value="3" type="radio">
                    30 ft.
                  </label> 
                  <label class="radio-inline" for="truck_size_4">
                    <input name="truck_size_id" id="truck_size_4" value="4" type="radio">
                    Tracker Trailer
                  </label> 
                </div>
              </div>

              <div class="form-group">
                <label class=" control-label" for="extra_stops">Do you need to make any extra stops?</label>
                <div class=""> 
                  <label class="radio-inline" for="1">
                    <input name="extra_stops" id="1" value="1" checked="checked" type="radio">
                      Yes
                  </label> 
                  <label class="radio-inline" for="0">
                    <input name="extra_stops" id="0" value="0" type="radio">
                    No
                  </label> 
                </div>
              </div>

              <div class="form-group">
                <span class="help-block small">*If yes, how many?</span>
                <div class=""> 
                  <label class="radio-inline" for="extra_stops_no_1">
                    <input name="extra_stops_no" id="extra_stops_no_1" value="1" checked="checked" type="radio">
                      1
                  </label> 
                  <label class="radio-inline" for="extra_stops_no_2">
                    <input name="extra_stops_no" id="extra_stops_no_2" value="2" type="radio">
                    2
                  </label> 
                  <label class="radio-inline" for="extra_stops_no_3">
                    <input name="extra_stops_no" id="extra_stops_no_3" value="3" type="radio">
                    3
                  </label> 
                </div>
              </div>

              <div class="form-group">
                <label class=" control-label" for="packing">Do you require packing services?</label>
                <div class=""> 
                  <label class="radio-inline" for="packing_1">
                    <input name="packing" id="packing_1" value="1" checked="checked" type="radio">
                      Yes
                  </label> 
                  <label class="radio-inline" for="packing-0">
                    <input name="packing" id="packing-0" value="0" type="radio">
                    No
                  </label> 
                </div>
              </div>

              <div class="form-group">
                <label class=" control-label" for="storage">Do you require storage?</label>
                <div class=""> 
                  <label class="radio-inline" for="storage_1">
                    <input name="storage" id="storage_1" value="1" checked="checked" type="radio">
                      Yes
                  </label> 
                  <label class="radio-inline" for="storage_0">
                    <input name="storage" id="storage_0" value="0" type="radio">
                    No
                  </label> 
                </div>
              </div>

              <div class="form-group">
                <label class=" control-label" for="moving_appliance">Are you moving any Appliances?</label>
                <div class=""> 
                  <label class="radio-inline" for="appliance_1">
                    <input name="moving_appliance" id="appliance_1" value="1" checked="checked" type="radio">
                      Yes
                  </label> 
                  <label class="radio-inline" for="appliance_0">
                    <input name="moving_appliance" id="appliance_0" value="0" type="radio">
                    No
                  </label> 
                </div>
              </div>

              <div class="form-group">
                <span class="help-block small">*If yes check all that apply</span>
                <div class="">
                  <select id="selectmultiple" name="moving_appliances" class="form-control" multiple="multiple">
                    <option value="1">Washer</option>
                    <option value="2">Dryer</option>
                    <option value="3">Stove</option>
                    <option value="4">Deep Freezer</option>
                    <option value="5">Refrigerator</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class=" control-label" for="assembly_reassembly">Do you require any Disassembly or Reassembly?</label>
                <div class=""> 
                  <label class="radio-inline" for="assembly_reassembly_1">
                    <input name="assembly_reassembly" id="assembly_reassembly_1" value="1" checked="checked" type="radio">
                      Yes
                  </label> 
                  <label class="radio-inline" for="assembly_reassembly_0">
                    <input name="assembly_reassembly" id="assembly_reassembly_0" value="0" type="radio">
                    No
                  </label> 
                </div>
              </div>

              <div class="form-group">
                <label class=" control-label" for="piano_gunsafe">Are you moving a Piano or Gun Safe?</label>
                <div class=""> 
                  <label class="radio-inline" for="piano_gunsafe_1">
                    <input name="piano_gunsafe" id="piano_gunsafe_1" value="1" checked="checked" type="radio">
                      Yes
                  </label> 
                  <label class="radio-inline" for="piano_gunsafe_0">
                    <input name="piano_gunsafe" id="piano_gunsafe_0" value="0" type="radio">
                    No
                  </label> 
                </div>
              </div>

              <div class="form-group">
                <div class="">
                  <a data-prev="step-2" href="#step-3" data-next="step-4" class="proceed-to-3 btn btn-success btn-lg btn-block">Proceed To Step 3</a>
                </div>
              </div>
            </fieldset>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>