<div class="row setup-content" id="step-4">
  <div class="col-xs-12">
    <div class="col-md-12">
      <div class="container">
        <div id="big-form" class="well auth-box">
            <fieldset>
              <h1 class="text-center">Terms and Conditions</h1><span class="text-center help-block">Effective 02/08/13</span>
              <div class="form-group">
                <div class="">
                  <div class="checkbox">
                    <label for="checkboxes-0">
                      <input name="checkboxes" id="checkboxes-0" value="1" type="checkbox">
                      By accepting services from Jacksonville Elite Movers or scheduling your move with us via online booking or over the phone booking you are agreeing to all of <u><a class="text-default" href="#policies" data-toggle="modal">Roovet International Corporation polices and procedures</a></u> as listed above by default all customers are still sent a copy via email to sign 98% of the time.
                      <br><br>
                      I have read and agreed to all the terms and conditions outlined in the <u><a class="text-default" href="#policies" data-toggle="modal">Roovet International Corporation polices and procedures</a></u>. I certify that i am authorized to enter into this agreement on my behalf or on behalf of my company. I understand that this is a legal binding agreement that may be enforced through legal action. By typing the name below, i am making a lawful electronic signature, capable of enforcement as if i had signed the agreement personally. 
                    </label>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="">
                  <input id="lic_issue" name="lic_issue" placeholder="Agreed On This Date" class="form-control input-md" type="text">
                </div>
              </div>

              <div class="form-group">
                <div class="">
                  <input id="textinput" name="textinput" placeholder="Authorized Signature (Sign your full name here)" class="form-control input-md" type="text">
                </div>
              </div>

              <div class="form-group">
                <div class="">
                  <a data-prev="step-4" href="#step-5" class="proceed-to-5 btn btn-success btn-lg btn-block">Proceed To Step 5</a>
                </div>
              </div>
            </fieldset>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>

<div class="modal" id="policies">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button class="close" aria-hidden="true" type="button" data-dismiss="modal">×</button>
        <h4 class="modal-title">Policies And Procedures</h4>
      </div>
      <div class="modal-body">

        <p class="text-uppercase"><em><strong><u>Please read carefully before using our moving services.</u></strong></em></p>

        <p>Roovet International Corporation Division of Jacksonville Elite Movers requires a Visa, MasterCard or American Express credit card at the time of scheduling a move to secure a date. If the customer will like to pay via cash upon driver arrival or Paypal Online Invoicing System they may do so by agreeing to our cancellation policies as well as all our other moving policies. The credit card number, expiration date and the security code are needed. If the customer needs to change the secured date or cancel the move completely, the following cancellation/rescheduling fees will be applied to the credit card number provided or collected by our collection agency Account Receivables:</p>
    
        <em><p><strong><u>Cancellation/Rescheduling Fee Policy</u></strong></p></em>
        <p><strong>Level 1: </strong>
        If the move is cancelled for any reason, the credit card will be charged a $200.00 cancellation fee. If no card is on file we will send you into collections for $400.00</p>

        <p><strong>Level 2: </strong>
        If the move is cancelled within seven days of the move, the credit card will be charged a $300.00 cancellation fee. If no card is on file we will send you into collections for $600.00</p>

        <p><strong>Level 3: </strong>
        If the move is rescheduled within 72 hours of the move, the credit card will be charged a $100.00 rescheduling fee. If no card is on file we will add it to your bill</p>

        <em><p><strong><u>Traveling & Fuel Surcharges</u></strong></p></em>

        <p>Traveling and Fuel Surcharges are non-refundable fees. We will not and do not issue any refunds for Traveling and Fuel Fees.</p>

        <p>There is a (3) two hr minimum on all jobs.</p>

        <p>Your privacy is very important to Roovet International Corporation. To help protect your privacy, we adhere to the following guidelines. This web site will explicitly ask when it needs information that personally identifies our customers or allows it to contact our customers (“Personal Information”). When possible, this web site will provide our customers with the means to make sure that Personal Information is correct and current.</p>

        <p>This web site and its service providers use Personal Information to operate the sites, provide services, and to inform our customers of new features, services, and products. This web site may also carefully select other companies to send our customers information about their products or services (a “Secondary Use”). If this web site intends to use Personal Information for a Secondary Use, we will not do so until we have provided our customers with an opportunity to affirmatively select such service.</p>

        <p>This web site may disclose Personal Information if required to do so by law or in the good-faith belief that such action is necessary to (a) comply with applicable law or with legal process served on Roovet International Corporation or the site; (b) protect and defend the rights or property of Roovet International Corporation or this site, and (c) act under exigent circumstances to protect the personal safety of users of Roovet International Corporation, the site, or the public. If at any time a customer believes that this web site has not adhered to these principles, please notify Roovet International Corporation.</p>

        <p>Our customers should also be aware that information and data may be automatically collected through the standard operation of our internet servers and through the use of “cookies”. “Cookies” are small text files a web site can use to recognize repeat users, facilitate the user’s ongoing access to and use of the site and allow a site to track usage behavior and compile aggregate data that will allow content improvements and targeted advertising. Cookies are not programs that come onto a system and damage files. Generally, cookies work by assigning a unique number to each customer that has no meaning outside the assigning site. If you do not want information collected through the use of cookies, there is a simple procedure in most browsers that allows a customer to deny or accept the cookie feature; however, you should note that cookies may be necessary to provide customers with certain features available on this web site.</p>

        <p><u><strong>E-mail</strong></u>: moving@roovet.com</p>

        <p><em><strong><u>Terms & Conditions</u></strong></em></p>

        <p>This agreement covers your shipment while in the care and custody of Roovet International Corporation (Roovet Corp.),. Roovet International Corporation. reserves the right to inspect furniture’s pre- and post-move condition. Any participation by the customer nullifies any coverage provided by Roovet Corp. Roovet Corp.. releases all liability for items placed in a vehicle not operated by a Roovet Corp. employee. Roovet Corp. is released from all liability once items are placed into any form of storage receptacle. A release must be signed before any item(s) are to be moved in any manner that is either harmful to the mover, the furniture or the surrounding area, i.e. door jamb, wall, etc. Roovet Corp. reserves the right to not move an item if it will cause harm to the item, the surrounding area or the person(s) moving the item.We do not ship any open boxes,bin, or containers etc. Roovet Corp. is not liable for damage to any flooring due to elements of weather, mud, dirt or damage to linoleum that has not been installed for more than 96 hours. Roovet Corp. is not liable for damage to pavement or concrete due to tire markings, ramp and lift gate scoring, or damage caused by steep approach angles. Roovet Corp. is not liable for damages due to load shift caused by steep driveways, uneven pavements or bumpy roads. Roovet Corp. is not liable for any boxed goods not professionally packed by Roovet Corp., or any glass items not professionally packed, i.e. mirrors, glass top tables, lamps, etc. Roovet Corp. is not liable for damage to mattresses due to bending or folding. Roovet Corp. is not liable for mechanical or electrical items, i.e. computers, copiers, televisions or appliances unless there is evidence of external damage. Roovet Corp. is not responsible for the disassemble or reassembly of electronics or pieces composed of composition (“particle”) board or veneer. Roovet Corp. is not liable for item(s) partially or totally made of composition board or veneer. Roovet Corp. is not liable for damage to pianos in any way. Roovet Corp. is not liable for items partially or totally made of marble, stone or slate. Roovet Corp. excludes any loss or damage to documents, software, currency, money, jewelry, watches or items of extraordinary value which are not brought to the attention of a Roovet Corp. office employee or are not listed specifically on the inventory prior to the move. The moving bill must be paid in full before any compensation is made. A claim must be brought to the attention of a Roovet International Corporation. representative within a period of no longer than 60 days from the completion of your move. Roovet Corp. reserves the right to appoint the company that does all repair work on any claim. Roovet International Corporation. reserves the right to dispute any claim to a court of law. Roovet Corp. reserves the right to change the start time of the move on the move date in the event that equipment or laborers are not available at a specific start time. Roovet Corp. reserves the right to change the day of the move due to inclement weather, acts of God or war. All balances must be paid in full, due upon completion of the move.</p>
      </div>
    </div>
  </div>
</div>