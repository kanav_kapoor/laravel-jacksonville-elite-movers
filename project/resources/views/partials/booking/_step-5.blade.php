<div class="row setup-content" id="step-5">
  <div class="col-xs-12">
    <div class="col-md-12">
      <div class="container">
        <div id="big-form" class="well auth-box">
            <fieldset>
              <h1 class="text-center">Authorization</h1>
              
              <div class="form-group">
                <div class="">
                  <div class="checkbox">
                    <label for="checkboxes-0">
                      <input name="checkboxes" id="checkboxes-0" value="1" type="checkbox">
                      Check this box If you would like to authorize Jacksonville Elite Movers to charge you card on file for any fee's you have not already paid including traveling and Fuel fee's 
                    </label>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="">
                  <div class="checkbox">
                    <label for="checkboxes-1">
                      <input name="checkboxes" id="checkboxes-1" value="2" type="checkbox">
                      Check this box If you would like to be added to our schedule for the date and time you selected 
                    </label>
                  </div>
                </div>
              </div>
              
              <div class="form-group">
                <div class="">
                  {!! Form::submit('Complete Your Booking', ['class' => 'btn btn-success btn-lg btn-block']) !!}
                  <!-- <button id="singlebutton" name="singlebutton" class="btn btn-success btn-lg btn-block">Complete Your Booking</button> -->
                </div>
              </div>
            </fieldset>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>
