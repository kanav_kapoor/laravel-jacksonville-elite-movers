<div class="row setup-content" id="step-1">
  <div class="col-xs-12">
    <div class="col-md-12">
      <div class="container">
        <div id="big-form" class="well auth-box">
            <fieldset>
              <h1 class="text-center">Customer Information</h1>
              <div class="form-group">
                <div class="">
                  <input id="first_name" name="first_name" placeholder="First Name" class="form-control input-md" type="text">
                </div>
                <p class="help-block"></p>
              </div>

              <div class="form-group">
                <div class="">
                  <input id="last_name" name="last_name" placeholder="Last Name" class="form-control input-md" type="text">
                </div>
              </div>

              <div class="form-group">
                <div class="">
                  <input id="email" name="email" placeholder="Email Address" class="form-control input-md" type="email">
                </div>
              </div>

              <div class="form-group">
                <div class="">
                  <input id="phone" name="phone" placeholder="Phone Number" class="form-control input-md" type="text">
                </div>
              </div>

              <div class="form-group">
                <label class=" control-label" for="radios">Best Time To Call</label>
                <div class="">
                  <select id="timings_id" name="timings_id" class="form-control">
                    <option value="1">Any</option>
                    <option value="2">Morning</option>
                    <option value="2">Afternoon</option>
                    <option value="2">Night</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <div class="">
                  <a data-prev="step-1" href="#step-2" data-next="step-3"class="proceed-to-2 btn btn-success btn-lg btn-block">Proceed To Step 2</a>
                </div>
              </div>
            </fieldset>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>
