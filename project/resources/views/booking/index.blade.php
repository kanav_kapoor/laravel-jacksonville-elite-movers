@extends('base')

@section('body')
<!-- Navbar -->
{{-- <div class="container-fluid">
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-2">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Jacksonville Elite Movers</a>
      </div>
  
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="navbar-collapse-2">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="#">Home</a></li>
          <li><a href="#">About</a></li>
          <li><a href="#">Services</a></li>
          <li><a href="#">Works</a></li>
          <li><a href="#">Contact</a></li>
          <li>
            <a class="btn btn-default btn-outline btn-circle"  data-toggle="collapse" href="#nav-collapse2" aria-expanded="false" aria-controls="nav-collapse2">Sign in</a>
          </li>
        </ul>
        <div class="collapse nav navbar-nav nav-collapse" id="nav-collapse2">
          <form class="navbar-form navbar-right form-inline" role="form">
            <div class="form-group">
              <label class="sr-only" for="Email">Email</label>
              <input type="email" class="form-control" id="Email" placeholder="Email" autofocus required />
            </div>
            <div class="form-group">
              <label class="sr-only" for="Password">Password</label>
              <input type="password" class="form-control" id="Password" placeholder="Password" required />
            </div>
            <button type="submit" class="btn btn-success">Sign in</button>
          </form>
        </div>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container -->
  </nav><!-- /.navbar -->
</div> --}}
<!--/ Navbar -->
<div class="container">
	<div class="row form-group steps-row">
		<div class="col-xs-12">
			<ul class="nav nav-pills nav-justified thumbnail setup-panel">
				<li class="active"><a href="#step-1" data-next="step-2">
					<h4 class="list-group-item-heading">Step 1</h4>
					<p class="list-group-item-text">Customer Information</p>
				</a></li>
				<li><a data-prev="step-1" href="#step-2" data-next="step-3">
					<h4 class="list-group-item-heading">Step 2</h4>
					<p class="list-group-item-text">Moving Details</p>
				</a></li>
				<li><a data-prev="step-2" href="#step-3" data-next="step-4">
					<h4 class="list-group-item-heading">Step 3</h4>
					<p class="list-group-item-text">Payment</p>
				</a></li>
				<li><a data-prev="step-3" href="#step-4" data-next="step-5">
					<h4 class="list-group-item-heading">Step 4</h4>
					<p class="list-group-item-text">Terms & Conditions</p>
				</a></li>
				<li><a data-prev="step-4" href="#step-5">
					<h4 class="list-group-item-heading">Step 5</h4>
					<p class="list-group-item-text">Authorization</p>
				</a></li>
			</ul>
		</div>
	</div>
	<section class="auth">
		{!! Form::open(['action' => 'BookingController@postStore']) !!}
			@include('partials.booking._step-1')
			@include('partials.booking._step-2')
			@include('partials.booking._step-3')
			@include('partials.booking._step-4')
			@include('partials.booking._step-5')
		{!! Form::close() !!}
	</section>
</div>
@stop


@section('css')
	<link rel="stylesheet" href="{{asset('css/app.css')}}">
@stop

@section('js')
	<script type="text/javascript" src="{{asset('js/transition.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/app.js')}}"></script>
@stop