<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('timings_id')->unsigned();
            $table->integer('affliate_id')->unsigned();
            $table->integer('weight_id')->unsigned();
            $table->integer('truck_size_id')->unsigned();
            $table->integer('card_id')->unsigned();
            $table->integer('license_id')->unsigned();
            $table->string('affliate_details')->nullable();
            $table->date('moving_date');
            $table->time('moving_time');
            $table->date('moving_date_alt');
            $table->time('moving_time_alt');
            $table->string('origin_address');
            $table->string('dest_address');
            $table->tinyInteger('movers_no');
            $table->boolean('extra_stops');
            $table->tinyInteger('extra_stops_no')->nullable();
            $table->boolean('packing');
            $table->boolean('storage');
            $table->boolean('moving_appliance');
            $table->boolean('assembly_reassembly');
            $table->boolean('piano_gunsafe');
            $table->date('tos_agreement_date');
            $table->string('tos_agreement_signature');
            $table->boolean('authorized_charge');
            $table->boolean('added_to_schedule');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bookings');
    }
}
