$(function() {
    //Floatlabel
    $('input').floatlabel();
    $('a, button').click(function(e){
        e.preventDefault();
    });

    var allSteps = $('ul.setup-panel li a, [class*="proceed-to-"]'),
        allPanels = $('.setup-content');

    allPanels.hide().first().show();

    allSteps.click(function(e)
    {
        e.preventDefault();
        $this = $(this);
        hasErrors = validatePanel($this.data('prev'));
        console.log(hasErrors);
        if(hasErrors) return false;
        showPanel($this.attr('href'));
    });

    function showPanel(panelIdSelector){
        var $selectedPanel = $(panelIdSelector),
            $selectedStep = allSteps.filter("[href='"+panelIdSelector+"']").closest('li');

        if (!$selectedStep.hasClass('disabled')) {
            allSteps.closest('li').removeClass('active').filter($selectedStep).addClass('active');
            allPanels.hide().filter($selectedPanel).show();
        }
    }

    function validatePanel(panelId){
        var hasErrors = false,
            $selectedPanel = $("#"+panelId);

        // validate text
        $selectedPanel.find("input[type='text'], input[type='email'], textarea").each(function(){
            var $val = $.trim($(this).val());
            hideError($(this));

            if($val == "") {
                hasErrors = true;
                showError($(this), 'empty');
            }else if ($val.length < 4 || $val.length > 100) {
                hasErrors = true;
                showError($(this), 'length');
            }else {
                
            }
        });

        return hasErrors;
    }

    function showError($selector, type){
        var message;
        switch(type) {
            case 'empty':
            message = 'This field is required';
            break;

            case 'length':
            message = 'This length of field must be between 4 and 100 characters';
            break;

        }

        displayError($selector, message);
    }

    function displayError($selector, message){
        $selector.after('<p class="help-block">'+message+'</p>');
    }

    function hideError($selector){
        $nextSelector = $selector.next();
        if($nextSelector.hasClass('help-block')) $nextSelector.remove();
    }

    /*$('[class*="proceed-to-"]').on('click', alert('working'))
    
    function showTab(tab){
        console.log(tab);
    }
    
    $('ul.setup-panel li.active a').trigger('click');*/
    
    // DEMO ONLY //
    /*$('#activate-step-2').on('click', function(e) {
        $('ul.setup-panel li:eq(1)').removeClass('disabled');
        $('ul.setup-panel li a[href="#step-2"]').trigger('click');
        $(this).remove();
    })  */ 

    /* Begin Step 1 */
    /*$("[id^='step-'] [class*='proceed-to-']").on('click', function(){
        var err = false;

        var $fieldset = $(this).closest('fieldset');
        $fieldset.find("[type='text']").each(function(){
            var $this = $(this);
            var fieldVal = $.trim($this.val());
            if(fieldVal == "") err = true
        });
        if(err == true) return false;
    });*/

    /* End Step 1 */

    /* Begin Step 2 */
    // Alternate Date
    $('#moving_date, #moving_date_alt, #lic_issue, #lic_exp').datetimepicker({
        format: 'D-MM-YYYY',
    });

    // Pickup Time
    $('#moving_time, #moving_time_alt').datetimepicker({
        format: 'HH:mm',
    });
    /* End Step 2 */

    /* Begin Step 3 */
    // Card Exp
    $('#card_exp').datetimepicker({
        format: 'MM/YY',
    });

    /* End Step 3 */
});