<?php
/*
users
-----
id
first_name - varchar(50)
last_name - varchar(50)
email - varchar(255)
phone - varchar(30)

bookings
--------
id
user_id
timings_id
affliate_id
weight_id
truck_size_id
card_id
license_id
affliate_details - varchar(50) - nullable
moving_date - date - (Fri-Sat) = $25.00 Weekend Fee)
moving_time - time
moving_date_alt - date
moving_time_alt - time
origin_address - varchar (255)
dest_address - varchar (255)
movers_no - tinyint
extra_stops - boolean - ($25.00 per stop)
extra_stops_no - tinyint, nullable
packing - boolean
storage - boolean
moving_appliance - boolean ($25.00 per appliance)
assembly_reassembly - boolean ($25.00)
piano_gunsafe - boolean ($70.00)
tos_agreement_date
tos_agreement_signature
authorized_charge
added_to_schedule

timings
-------
id
name

affliates
---------
id
name


Weights
-------

id
name
weight - nullable

cards
-------
id
first_name
last_name
card_type_id
card_no
card_expiry
cvv
address_billing
email
phone

card_types
----------
id
name

license
-------
id
license_no
state
date_issue
date_exp

truck_sizes
-----------

id
name

moving_appliances
-----------------

id
name

appliance_booking
------------------
id
appliance_id
booking_id
*/