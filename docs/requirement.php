<?php
/*
Customers can choose to book there move online by following the simple steps

Step 1.

Customer Information :

First Name :
Last Name :
Email Address :
Phone Number :
Best Time To Call : (Any) (Morning) (Afternoon) (Night)
Will you like to get notified of deals : (Yes) (No)

Step 2.

Moving Details :

How did you hear about us ? : (Groupon) =value= Please enter voucher number  (Living Social) =value= Please enter voucher number (Experience) =value= Please enter voucher number (Google) (A Friend or Co-worker) - Please enter friends name or co-worker (Other)

What day would you like to move ? = please select a day (Sun-Thrus) = No additional fee's (Fri-Sat) = $25.00 Weekend Fee

If your date is not available what day will like like as a 2nd option ?   (Date)

We always give our drivers a 2 hour window to arrival at your current address 

What time will you like to move ? : select a time = value= add two hours to selected time

Origin Address :

Destination Address :

Estimated Weight : (Studio Apt. = 1,500 lbs) ( 1 bedroom = 3,000 lbs) (2 bedroom = 6,000 lbs) (3 bedroom = 9,000 lbs) (3 bedroom + Garage  = 10,000 lbs) ( 4+ Bedrooms)

How many mover do you require ? : (1 )( 2)  (3) (4) (5) (6) (7)

What size truck do you require ? : (26ft ) (28ft) (30ft) (Tracker Trailer)

Do you need to make any extra stops : (Yes) - If Yes how many ? : (1) (2) (3) =value= ($25.00 per stop)  (No)

Do you require packing services ? : (Yes)  (No)

Do you require storage ? : (Yes)  (No)

Are you moving any Appliances : (Yes) - If yes check all that apply : (Washer) (Dryer) (Stove) (Deep Freezer) (Refrigerator) value= $25.00    (No)

Do you require any Disassembly or Reassembly : (Yes)  (No) =value= ($25.00)

Are you moving a Piano or Gun Safe ? : (Yes) (No) =value= ($70.00)



Step. 3

Add a valid Major Credit Card or Debit Card on File :
 
We do not accept prepaid cards

Information must match your card

First Name :
Last Name :
Card Type : (Visa) (Mastercard) (Amex) Value= We do not accept (Discover) Value= We do not accept
Card Number : Value= 16 numbers
Exp. Date : Value= 4 numbers
CVC : Value= 3 numbers
Billing Address :
Email Address :
Phone Number :

Driver License of person paying for the move  (Driver Will Verify)
Driver License # :
State :
Exp. Date :
Issue Date :


Step. 4

Terms and Conditions :

Effective 02/08/13

Roovet International Corporation. is dedicated to providing safe and efficient moves to each of its valued customers. To do this, the company has put some policies in place that will enable each customer to have the smoothest move possible. Each policy is detailed below. Out move coordinators are available to answer any questions you have about any of the following information.

PLEASE READ CAREFULLY BEFORE
USING OUR MOVING SERVICES.

There is a (3) two hr minimum on all jobs.

Your privacy is very important to Roovet International Corporation. To help protect your privacy, we adhere to the following guidelines. This web site will explicitly ask when it needs information that personally identifies our customers or allows it to contact our customers (“Personal Information”). When possible, this web site will provide our customers with the means to make sure that Personal Information is correct and current.

This web site and its service providers use Personal Information to operate the sites, provide services, and to inform our customers of new features, services, and products. This web site may also carefully select other companies to send our customers information about their products or services (a “Secondary Use”). If this web site intends to use Personal Information for a Secondary Use, we will not do so until we have provided our customers with an opportunity to affirmatively select such service.

This web site may disclose Personal Information if required to do so by law or in the good-faith belief that such action is necessary to (a) comply with applicable law or with legal process served on Roovet International Corporation or the site; (b) protect and defend the rights or property of Roovet International Corporation or this site, and (c) act under exigent circumstances to protect the personal safety of users of Roovet International Corporation, the site, or the public. If at any time a customer believes that this web site has not adhered to these principles, please notify Roovet International Corporation.

Our customers should also be aware that information and data may be automatically collected through the standard operation of our internet servers and through the use of “cookies”. “Cookies” are small text files a web site can use to recognize repeat users, facilitate the user’s ongoing access to and use of the site and allow a site to track usage behavior and compile aggregate data that will allow content improvements and targeted advertising. Cookies are not programs that come onto a system and damage files. Generally, cookies work by assigning a unique number to each customer that has no meaning outside the assigning site. If you do not want information collected through the use of cookies, there is a simple procedure in most browsers that allows a customer to deny or accept the cookie feature; however, you should note that cookies may be necessary to provide customers with certain features available on this web site.

E-mail: moving@roovet.com

Terms & Conditions
This agreement covers your shipment while in the care and custody of Roovet International Corporation (Roovet Corp.),. Roovet International Corporation. reserves the right to inspect furniture’s pre- and post-move condition. Any participation by the customer nullifies any coverage provided by Roovet Corp. Roovet Corp.. releases all liability for items placed in a vehicle not operated by a Roovet Corp. employee. Roovet Corp. is released from all liability once items are placed into any form of storage receptacle. A release must be signed before any item(s) are to be moved in any manner that is either harmful to the mover, the furniture or the surrounding area, i.e. door jamb, wall, etc. Roovet Corp. reserves the right to not move an item if it will cause harm to the item, the surrounding area or the person(s) moving the item.We do not ship any open boxes,bin, or containers etc. Roovet Corp. is not liable for damage to any flooring due to elements of weather, mud, dirt or damage to linoleum that has not been installed for more than 96 hours. Roovet Corp. is not liable for damage to pavement or concrete due to tire markings, ramp and lift gate scoring, or damage caused by steep approach angles. Roovet Corp. is not liable for damages due to load shift caused by steep driveways, uneven pavements or bumpy roads. Roovet Corp. is not liable for any boxed goods not professionally packed by Roovet Corp., or any glass items not professionally packed, i.e. mirrors, glass top tables, lamps, etc. Roovet Corp. is not liable for damage to mattresses due to bending or folding. Roovet Corp. is not liable for mechanical or electrical items, i.e. computers, copiers, televisions or appliances unless there is evidence of external damage. Roovet Corp. is not responsible for the disassemble or reassembly of electronics or pieces composed of composition (“particle”) board or veneer. Roovet Corp. is not liable for item(s) partially or totally made of composition board or veneer. Roovet Corp. is not liable for damage to pianos in any way. Roovet Corp. is not liable for items partially or totally made of marble, stone or slate. Roovet Corp. excludes any loss or damage to documents, software, currency, money, jewelry, watches or items of extraordinary value which are not brought to the attention of a Roovet Corp. office employee or are not listed specifically on the inventory prior to the move. The moving bill must be paid in full before any compensation is made. A claim must be brought to the attention of a Roovet International Corporation. representative within a period of no longer than 60 days from the completion of your move. Roovet Corp. reserves the right to appoint the company that does all repair work on any claim. Roovet International Corporation. reserves the right to dispute any claim to a court of law. Roovet Corp. reserves the right to change the start time of the move on the move date in the event that equipment or laborers are not available at a specific start time. Roovet Corp. reserves the right to change the day of the move due to inclement weather, acts of God or war. All balances must be paid in full, due upon completion of the move.





Policy and Procedures :

Scheduling Policy
At the time a customer schedules his or her move, he or she will be given a move date. 24 hours prior to the scheduled date, your move coordinator will call you to confirm the move, as well as provide you with a window of arrival.

If a customer’s circumstances require him or her to be the first move of the day, he or she can secure that arrival time by taking advantage of the Priority Placement Program.

A $25 fee will guarantee that the customer will be the first of the day. His or her move coordinator will still call the customer 24 hours prior to the move date to confirm.

Cancellation/Rescheduling Fee Policy
Roovet International Corporation Division of Jacksonville Elite Movers requires a Visa, MasterCard or American Express credit card at the time of scheduling a move to secure a date.If the customer will like to pay via cash upon driver arrival or Paypal Online Invoicing System they may do so by agreeing to our cancellation policies as well as all our other moving policies.The credit card number, expiration date and the security code are needed. If the customer needs to change the secured date or cancel the move completely, the following cancellation/rescheduling fees will be applied to the credit card number provided or collected by our collection agency Account Receivables:

Level 1: If the move is cancelled for any reason, the credit card will be charged a $200.00 cancellation fee.If no card is on file we will send you into collections for $400.00

Level 2: If the move is cancelled within seven days of the move, the credit card will be charged a $300.00 cancellation fee.If no card is on file we will send you into collections for $600.00

Level 3: If the move is rescheduled within 72 hours of the move, the credit card will be charged a $100.00 rescheduling fee.If no card is on file we will add it to your bill

If the move is rescheduled after a Level 2 cancellation, the card will be credited $100.00, effectively turning the cancellation fee into a rescheduling fee.If no card is on file we will add it to your bill.

Traveling & Fuel Surcharges

Traveling and Fuel Surcharges are non-refundable fees. We will not and do not issue any refunds for Traveling and Fuel Fees.

By accepting services from Jacksonville Elite Movers or scheduling your move with us via online booking or over the phone booking  you are agreeing to all of Roovet International Corporation polices and procedures as listed above by default all customers are still sent a copy via email to sign 98% of the time.


Step 5.

Check this box If you would like to authorize Jacksonville Elite Movers to charge you card on file for any fee's you have not already paid including traveling and Fuel fee's


Check this box If you would like to be added to our schedule for the date and time you selected


Click Process when your ready to complete your booking = If Clicked Value= Thank you for choosing Jacksonville Elite Movers to preform you upcoming move for you we look forward to servicing you.


*/
